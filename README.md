Catatan dari bootcamp online [dicoding](https://www.dicoding.com/dashboard)

Prerequisite :
- nodejs (12+)
- npm

Eksplor folder yang merupakan masing - masing materi.
Bila ada `package.json` jangan lupa untuk menginstallnya dengan
`cd <folder>` lalu `npm install`
const getCoffee = () => {
    return new Promise((resolve, reject) => {
        const seeds = 1
        setTimeout(() => {
            if (seeds >= 10) {
                resolve("Kopi didapatkan")
            } else {
                reject("Biji kopi habis!")
            }
        }, 1000)
    })
}

// promise
// function makeCoffee() {
//     const coffee = getCoffee().then(coffee=> console.log(coffee))
// }

// makeCoffee()

// Async Await
async function makeCoffee() {
    try {
        const coffee = await getCoffee()
        console.log(coffee)
    } catch (rejectedReason) {
        console.log(rejectedReason)
    }
}

// makeCoffee()

const checkAvailability = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!state.isCoffeeMachineBusy) {
                resolve("Mesin kopi siap digunakan")
            } else {
                reject("Maaf, mesin sedang sibuk")
            }
        }, 1000)
    })
}

const state = {
    stock: {
        coffeeBeans: 250,
        water: 100
    },
    isCoffeeMachineBusy: false
}

const checkStock = () => {
    return new Promise((resolve, reject) => {
        state.isCoffeeMachineBusy = true
        setTimeout(() => {
            if (state.stock.coffeeBeans >= 16 && state.stock.water >= 50) {
                resolve("Stock cukup. Bisa membuat kopi")
            } else {
                reject ("Stock tidak cukup")
            }
        }, 1500)
    })
}

const brewCoffee = () => {
    console.log("Membuat kopi Anda")
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Kopi sudah siap!")
        }, 2000)
    })
}

const boilWater = () => {
    return new Promise((resolve, reject) => {
        console.log("Memanaskan air...")
        setTimeout(() => {
            resolve("Air panas sudah siap")
        }, 2000)
    })
}

const grindCoffeeBeans = () => {
    return new Promise((resolve, reject) => {
        console.log("Menggiling biji kopi...")
        setTimeout(() => {
            resolve("Kopi sudah siap")
        }, 1000)
    })
}

async function makeEkspresso() {
    try {
        const availability = await checkAvailability()
        console.log(availability)
        const stock = await checkStock()
        console.log(stock)
        const promiseProcess = await Promise.all([boilWater(), grindCoffeeBeans()])
        console.log(promiseProcess)
        const coffee = await brewCoffee()
        console.log(coffee)
    } catch (rejectedReason) {
        console.log(rejectedReason)
    }
}

makeEkspresso()
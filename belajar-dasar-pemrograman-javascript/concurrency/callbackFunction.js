const orderCoffee = (callback) => {
    let coffee = null
    console.log("Sedang membuat kopi, silakan tunggu...")
    setTimeout(() => {
        coffee = "Kopi sudah jadi"
        callback(coffee)
    }, 2000)
}

const coffee = orderCoffee(coffee => console.log(coffee))
const checkAvailability = () => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (!state.isCoffeeMachineBusy) {
                resolve("Mesin kopi siap digunakan")
            } else {
                reject("Maaf, mesin sedang sibuk")
            }
        }, 1000)
    })
}

const state = {
    stock: {
        coffeeBeans: 250,
        water: 100
    },
    isCoffeeMachineBusy: false
}

const checkStock = () => {
    return new Promise((resolve, reject) => {
        state.isCoffeeMachineBusy = true
        setTimeout(() => {
            if (state.stock.coffeeBeans >= 16 && state.stock.water >= 50) {
                resolve("Stock cukup. Bisa membuat kopi")
            } else {
                reject ("Stock tidak cukup")
            }
        }, 1500)
    })
}

const brewCoffee = () => {
    console.log("Membuat kopi Anda")
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Kopi sudah siap!")
        }, 2000)
    })
}

function makeEkspresso() {
    checkAvailability()
    .then((value) => {
        console.log(value)
        return checkStock()
    })
    .then((value) => {
        console.log(value)
        return brewCoffee()
    })
    .then((value) => {
        console.log(value)
    })
    .catch((rejectedReason) => {
        console.log(rejectedReason)
    })
}

makeEkspresso()
const _ = require('lodash')

const myArray = [1, 2,3,4]
let sum = 0

// for (let i=0; i< myArray.length; i++) {
//     sum += myArray[i]
// }

// pakai reduce
// sum = myArray.reduce((prev, curr) => {
//     return prev + curr
// })

// pakai lodash
sum = _.sum(myArray)

console.log(sum)
Data Structure
- Object
    - object.key
    - object['key'] 
- Array
	- array.push()
	- array.pop()
	- array.unshif()
	- array.shift()
	- delete array[i]
	- array.splice(mulai, akhir)
	- spread operator
		- const favorite = [...favorites] = favorites[0] + favorites[1] + dst
		- const favorite = [favorites + other] = favorites[0], favorites[1], other[0], dst
		- spread operator = melebur array
	- desctructuring
		-object
			- const {firstname, lastname} = profile
			- default value: const {firstname='Ama'} = profile
			- different local variable: let {firstName: localFirstName,age: localAge} = profile;
		-array
			- const [firstFood, secondFood, thirdFood, fourthFood] = favorites;
			- const [, , thirdFood ] = favorites;
			- [myFood, herFood] = favorites;
			- [a, b] = [b, a]
- Map
	- const myMap = new Map([['1', 'a String key'],[1, 'a number key'],[true, true]]);
	- myMap.get(1)
	- myMap.set(2, "lalala")
- Set
	- const numberSet = new Set([1, 4, 6, 4, 1]);
	- numberSet.add(24)
	- numberSet.delete(4)
	
Object Oriented Programming
- Pilar OOP
	- encapsulation
	- abstraction
	- inheritance
	- polymorphism
- static method
	- membuat method tanpa instantiate
- ada 3 cara pembuatan kelas
	- class method
	- statement function
	- cara baru 	
- super() digunakan untuk memanggil constructor parent, hanya dapat digunakan di constructor
- super.methodName(..) digunakan untuk memanggil parent method



Functional Programming
- adalah
	- paradigma pemrograman di mana proses komputasi di dasarkan pada fungsi matematika murni
- Konsep
	- Pure function
		- mengembalikan nilai yang sama bila inputannya (param) sama
		- hanya bergantung pada argumen yang diberikan
		- tidak menimbulkan efek samping
	- Immutability
		- objek tidak boleh diubah setelah objek tersebut dibuat
	- High Order Function
		- fungsi yang dapat menerima fungsi lainnya pada argumen, mengembalikan sebuah fungsi, atau keduanya.
		- digunakan untuk
			- mengabstraksi atau mengisolasi sebuah aksi, event, atau menangani alur asynchronous menggunakan callback, promise, dll
			- membuat utilites yang dapat digunakan diberbagai tipe data
			- membuat teknik currying atau function composition
- Reusable function
	- Array Map
	- Array filter
	- Array forEarch 
	

Loop
	- for
	- while
	- do while
	- forEach (tdk bisa continue, dan break)
	- for .. of ..
	- for .. in ..
	
Module
- Export
	- module.exports = { }; require("..");
- ES6 Module
	- export default ...; import ... from "..";

Testing
- Pengujian pengembangan perangkat lunak
	- static test
		- typo
		- error types
	- unit test
		- fungsi
		- kelas, dll
	- integration test
		- script test
	- end-to-end test
		- flow dari awal sampai akhir
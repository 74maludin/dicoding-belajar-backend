const { nanoid } = require('nanoid');
const books = require('./books');

const addBookHandler = (request, h) => {
  const {
    name, year, author, summary, publisher, pageCount, readPage, reading,
  } = request.payload;

  const id = nanoid(16);
  const finished = pageCount === readPage;
  const insertedAt = new Date().toISOString();
  const updatedAt = insertedAt;

  const newBook = {
    id,
    name,
    year,
    author,
    summary,
    publisher,
    pageCount,
    readPage,
    finished,
    reading,
    insertedAt,
    updatedAt,
  };

  if (!name) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal menambahkan buku. Mohon isi nama buku',
    });

    response.code(400);
    return response;
  }

  if (readPage > pageCount) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal menambahkan buku. readPage tidak boleh lebih besar dari pageCount',
    });

    response.code(400);
    return response;
  }

  books.push(newBook);
  const isSuccess = books.filter((book) => book.id === id).length > 0;

  if (isSuccess) {
    const response = h.response({
      status: 'success',
      message: 'Buku berhasil ditambahkan',
      data: {
        bookId: id,
      },
    });
    response.code(201);
    return response;
  }

  const response = h.response({
    status: 'error',
    message: 'Buku gagal ditambahkan',
  });
  response.code(500);
  return response;
};

const getAllBooksHandler = (request, h) => {
  let { name, reading, finished } = request.query;
  name = name || '';
  reading = reading || '';
  finished = finished || '';

  if (name !== '') {
    name = name.toLowerCase();
    const patt = new RegExp(name, 'i');
    const someBook = books.filter((n) => n.name.match(patt));
    const nameBook = [];
    someBook.forEach((book) => {
      const newBook = {
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      };
      nameBook.push(newBook);
    });

    const response = h.response({
      status: 'success',
      data: {
        books: nameBook,
      },
    });
    response.code(200);
    return response;
  }

  if (reading !== '') {
    reading = Boolean(Number(reading));
    const someBook = books.filter((n) => n.reading === reading);
    const readingBook = [];
    someBook.forEach((book) => {
      const newBook = {
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      };
      readingBook.push(newBook);
    });

    const response = h.response({
      status: 'success',
      data: {
        books: readingBook,
      },
    });
    response.code(200);
    return response;
  }

  if (finished !== '') {
    finished = Boolean(Number(finished));
    const someBook = books.filter((n) => n.finished === finished);
    const finishBook = [];
    someBook.forEach((book) => {
      const newBook = {
        id: book.id,
        name: book.name,
        publisher: book.publisher,
      };
      finishBook.push(newBook);
    });

    const response = h.response({
      status: 'success',
      data: {
        books: finishBook,
      },
    });
    response.code(200);
    return response;
  }

  const someBook = [];
  books.forEach((book) => {
    const newBook = {
      id: book.id,
      name: book.name,
      publisher: book.publisher,
    };
    someBook.push(newBook);
  });
  const response = h.response({
    status: 'success',
    data: {
      books: someBook,
    },
  });
  response.code(200);
  return response;
};

const getBookByIdHandler = (request, h) => {
  const { id } = request.params;

  const book = books.filter((n) => n.id === id)[0];

  if (book !== undefined) {
    const response = h.response({
      status: 'success',
      data: {
        book,
      },
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Buku tidak ditemukan',
  });
  response.code(404);
  return response;
};

const editBookByIdHandler = (request, h) => {
  const { id } = request.params;

  const {
    name, year, author, summary, publisher, pageCount, readPage, reading,
  } = request.payload;
  const finished = pageCount === readPage;
  const updatedAt = new Date().toISOString();

  if (!name) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal memberbarui buku. Mohon isi nama buku',
    });

    response.code(400);
    return response;
  }

  if (readPage > pageCount) {
    const response = h.response({
      status: 'fail',
      message: 'Gagal memperbarui buku. readPage tidak boleh lebih besar dari pageCount',
    });

    response.code(400);
    return response;
  }

  const index = books.findIndex((note) => note.id === id);

  if (index !== -1) {
    books[index] = {
      ...books[index],
      name,
      year,
      author,
      summary,
      publisher,
      pageCount,
      readPage,
      finished,
      reading,
      updatedAt,
    };

    const response = h.response({
      status: 'success',
      message: 'Buku berhasil diperbarui',
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Gagal memperbarui catatan. Id tidak ditemukan',
  });
  response.code(404);
  return response;
};

const deleteBookByIdHandler = (request, h) => {
  const { id } = request.params;

  const index = books.findIndex((book) => book.id === id);

  if (index !== -1) {
    books.splice(index, 1);
    const response = h.response({
      status: 'success',
      message: 'Buku berhasil dihapus',
    });
    response.code(200);
    return response;
  }

  const response = h.response({
    status: 'fail',
    message: 'Buku gagal dihapus. Id tidak ditemukan',
  });
  response.code(404);
  return response;
};

module.exports = {
  addBookHandler,
  getAllBooksHandler,
  getBookByIdHandler,
  editBookByIdHandler,
  deleteBookByIdHandler,
};

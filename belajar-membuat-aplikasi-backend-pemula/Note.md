REST API

API
    - Web Service
    - SDK

Sifat kunci dari REST API:
    - client-server
    - stateless
    - cacheable
    - layered

Empat poin REST API:
    - Format request dan response
        - JSON
        - XML
    - HTTP Verbs/Methods
        - GET
        - POST
        - PUT
        - DELETE
    - HTTP Response code
        - 200, 201, 400, 401, 403, 404, 500
    - URL Design (URL/Path/Endpoint)
        - gunakan kata sifat/benda daripada kata kerja
        - gunakan kata jamak untuk resource collection
        - gunakan endpoint berantai untuk resource yang memiliki hirariki/relasi

Nodejs Global Object
    - member global (Objec.getOwnPropertyNames(window|global))
        - browser : window
        - nodejs : global
    - global object penting
        - process

Modul pada Nodejs
    - local module
    - core module
    - third party module

http.clientRequest turunan dari readableStream

Routing : menentukan respon server berdasarkan path/ur yang diminta client

Web Framework : kerangka yang dapat membantu mempermudah pengembangan web termasuk dalam membuat web server.
Menyederhanakan pengembangan web dalam hal:
- pembuatan server
- routing
- menangani permintaan
- interaksi dengan database
- otorisasi
- mingkatkan ketahanan web dari serangan luar

konfigurasi awal eslint : `npx eslint --init`

prinsip single responsibility approach : gunakan satu berkas Javascript untuk satu tujuan saja
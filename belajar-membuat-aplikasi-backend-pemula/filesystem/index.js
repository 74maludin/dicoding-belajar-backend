const fs = require('fs')
const folder = './filesystem'

const fileReadCallback = (error, data) => {
    if (error) {
        console.log('Gagal membaca berkas')
        return
    }

    console.log(data)
}

fs.readFile('./filesystem/notes.txt', 'UTF-8', fileReadCallback)
// console.log(fs.readFileSync('notes.txt', 'utf-8', fileReadCallback))

// untuk membaca list folder
// fs.readdir(folder, (err, files) => {
//     files.forEach(file => {
//         console.log(file)
//     })
// })
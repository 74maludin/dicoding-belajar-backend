const http = require('http')

const port = 5000
const host = 'localhost'

const requestListener = (request, response) => {
    const { method, url } = request

    response.setHeader('Content-Type', 'application/json')
    response.setHeader('X-Powered-By', 'NodeJS')

    if (url === '/') {

        if (method === 'GET') {
            response.statusCode = 200
            response.end(JSON.stringify({
                message: 'Ini adalah homepage'
            }))
        } else {
            response.statusCode = 400
            response.end(JSON.stringify({
                message: `Halaman tidak dapat diakses dengan ${method} request`
            }))
        }

    } else if (url === '/about') {

        if (method === 'GET') {
            response.statusCode = 200
            response.end(JSON.stringify({
                message: 'Halo! Ini adalah halaman about.'
            }))
            return
        } else if (method === 'POST') {
            let body = []

            request.on('data', (chunk) => {
                body.push(chunk)
            })

            request.on('end', () => {
                body = Buffer.concat(body).toString()
                const { name } = JSON.parse(body)

                response.statusCode = 200
                response.end(JSON.stringify({
                    message: `Halo, ${name}! Ini adalah halaman about.\n`
                }))
            })
            return
        } else {
            response.statusCode = 400
            response.end(JSON.stringify({
                message: `Halaman tidak dapat diakses dengan ${method} request\n`
            }))
        }

    } else {
        response.statusCode = 404
        response.end(JSON.stringify({
            message: 'Halaman tidak ditemukan\n'
        }))
    }

}

const server = http.createServer(requestListener)

server.listen(port, host, () => {
    console.log(`Server berjalan pada http://${host}:${port}`)
})